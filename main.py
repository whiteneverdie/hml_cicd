import time
from flask import Flask, jsonify    
import os
import json


SECRET_NUMBER=os.environ['SECRET_NUMBER']

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "<h1 style='color:blue'>Python Motherfucker, Do You Speak It?</h1>"


@app.route('/return_secret_number')
def return_secret_number():
    """
    """
    global SECRET_NUMBER
    time.sleep(1)
    return jsonify(SECRET_NUMBER)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=False, threaded = False)


