# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

ENV SECRET_NUMBER=$SECRET_NUMBER

WORKDIR /app
COPY . .
RUN pip3 install -r requirements.txt

ENTRYPOINT [ "python3", "main.py"]

